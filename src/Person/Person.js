/*jshint esversion: 6 */

import React from "react";
import "./Person.css";
import Radium from 'radium';
//import ReactDOM from "react-dom";

const person = props => {
    const style={
   '@media(min-width:500px':{
     width:'450px'
   }

 };
  return (
    <div className="Person" style={style} >
      <p onClick={props.click}> Hey From {props.name} {props.surname}. </p>
      <p>        {props.children}     </p>
      <input type="text" onChange={props.changed} value={props.name} />
    </div>
  );
};

export default Radium(person);