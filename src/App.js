import React, {
  Component
} from 'react';
import Radium,{StyleRoot} from 'radium';
import './App.css';
import Person from './Person/Person';


class App extends Component {
  state = {
    persons: [{
      id: '1',
      name: 'Megha',
      surname: 'Brahmbhatt'
    },
    {
      id: '2',
      name: 'shital',
      surname: 'Kher'
    },
    {
      id: '3',
      name: 'nenshi',
      surname: 'Patel'
    }
    ],
    showPerson: false
  }
  deletePerson = (personIndex) => {
    console.log("clicked.!");
    const persons = [...this.state.persons];

    persons.splice(personIndex, 1);
    this.setState({ persons: persons })

  }
  nameChangedHandler = (event, id) => {
    //console.log("clicked.!");
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });
    const person = {
      ...this.state.persons[personIndex]
    };
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({
      persons: persons

    });

  }
  togglePerson = (showPerson) => {
    //console.log("clicked");
    const doesShow = this.state.showPerson;
    //console.log(this.showPerson);
    this.setState({
      showPerson: !doesShow
    });


  }
  render() {
    const style = {
      backgroundColor: 'Green',
      color : 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer',
      ':hover':{
        backgroundColor:'lightgreen',
        color:'black'
      }
    };
    let persons = null;
    if (this.state.showPerson) {
      persons = (
        <div>
          {
            this.state.persons.map((person, index) => {
              return <Person

                name={person.name}
                surname={person.surname}
                key={person.id}

                changed={(event) => this.nameChangedHandler(event, person.id)}
                click={() => this.deletePerson(index)}  >
              </Person>
            })
          }
        </div>
      );
        style.backgroundColor='red';
        style[':hover']={
          backgroundColor:'salmon',
          color:'black'
        };
    }
    return (
      <StyleRoot>
      <div className="App" >

        <h1 > Hi from React </h1>
        <p> By Megha Brahmbhatt </p>
        <button style={style}
          onClick={this.togglePerson} > Toggle Persons </button>
        {persons}
      </div>
      </StyleRoot>
    );
  }
}

export default Radium(App);